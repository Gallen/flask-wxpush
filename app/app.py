from flask import Flask
from flask_restful import Api, Resource
from wxpush_resource import WXPushResource

app = Flask(__name__)
api = Api(app)

class HelloWorld(Resource):
    def get(self):
        return {'data': 'running'}

api.add_resource(HelloWorld, '/')

api.add_resource(WXPushResource, '/push')

if __name__ == '__main__':
    import os
    HOST = os.environ.get('SERVER_HOST', '0.0.0.0')
    try:
        PORT = int(os.environ.get('SERVER_PORT', '8000'))
    except ValueError:
        PORT = 8000
    app.run(HOST, PORT, debug=True)