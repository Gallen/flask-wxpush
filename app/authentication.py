from flask_httpauth import HTTPBasicAuth
from config import Config

auth = HTTPBasicAuth()

user = Config.BASIC_AUTH_USERNAME
pwd = Config.BASIC_AUTH_PASSWORD

USER_DATA = {}
USER_DATA[user] = pwd

@auth.verify_password
def verify(username, password):
    if not (username and password):
        return False
    return USER_DATA.get(username) == password