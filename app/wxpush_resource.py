from config import Config
from flask_restful import Resource, request, reqparse
from authentication import auth
from corpwechatbot.app import AppMsgSender

class WXPushResource(Resource):
    msg_type = "text"
    msg_data = {}

    def send(self):
        corpid=Config.CORPID
        corpsecret=Config.CORPSECRET
        agentid=Config.AGENTID

        print('env info: ', corpid, agentid, corpsecret)

        app = AppMsgSender(corpid=corpid,  # 你的企业id
                            corpsecret=corpsecret,  # 你的应用凭证密钥
                            agentid=agentid)   # 你的应用id

        print('msg info: ', self.msg_type, self.msg_data)

        func = app.__getattribute__('send_' + self.msg_type)
        is_safe = 0
        if self.msg_type == 'mpnews':
            is_safe = 1

        func(content=self.msg_data.get("content", "content"),
            title=self.msg_data.get("title", "title"),
            desp=self.msg_data.get("desp", "desp"),
            url=self.msg_data.get("url", "url"),
            picurl=self.msg_data.get("picurl", "picurl"),
            image_path=self.msg_data.get("image_path", "image_path"),
            content_source_url=self.msg_data.get("content_source_url", "content_source_url"),
            author=self.msg_data.get("author", "author"),
            digest=self.msg_data.get("digest", "digest"),
            btntxt=self.msg_data.get("btntxt", "btntxt"),
            safe=is_safe,
        )

    @auth.login_required
    def post(self):
        parser = reqparse.RequestParser()
        # type: text, markdown, news, mpnews, card
        parser.add_argument('type', location='form', type=str, required=True, help="Type cannot be blank!")
        args = parser.parse_args()
        self.msg_type = args.get('type')

        self.msg_data = request.form

        try:
            self.send()
            return {"data": "success"}
        except Exception as ex:
            print("error on send: %s" %ex)
            return {"data": "failed"}
