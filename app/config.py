import os


class Config():
    BASIC_AUTH_USERNAME = os.environ.get('BASIC_AUTH_USERNAME', 'user')
    BASIC_AUTH_PASSWORD = os.environ.get('BASIC_AUTH_PASSWORD', 'password')
    CORPID=os.environ.get('CORPID', 'CORPID')
    CORPSECRET=os.environ.get('CORPSECRET', 'CORPSECRET')
    AGENTID=os.environ.get('AGENTID', 'AGENTID')
