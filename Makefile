default:
	touch .gitignore

nvm:
	node -v > .nvmrc

docker:
	touch Dockerfile
	touch .dockerignore

md:
	touch README.md

image:
	docker build -t flask-wxpush .
