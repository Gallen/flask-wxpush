# Flask-WXPush

通过企业微信和 Restful 接口发送推送到微信

* python 3.8+

## Env

see [config.py](./app/config.py)

## Start

```sh
pip install -r requirements.txt
python app/app.py
```

```sh
curl -X POST -u user:password -d type='text' -d content='message content' 'http://localhost:8000/push'
```

## Server

```sh
docker run -d -p 8000:8000 \
    --name wxpush \
    -e BASIC_AUTH_USERNAME=user \
    -e BASIC_AUTH_PASSWORD=password \
    -e CORPID=corpid \
    -e CORPSECRET=corpsecret \
    -e AGENTID=agentid \
    hvanke/flask-wxpush
```
